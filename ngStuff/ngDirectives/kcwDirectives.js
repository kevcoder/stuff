angular.module('kcw.directives', [])
    .directive('kcwQueryBuilder', function() {
        return {
            restrict: 'AE',
            template: '<div class="kcw-jqbuilder"></div>' +
                '<div class="btn-group" role="group" aria-label="..."> ' +
                '<button class="kcw-jqbuilder-clear btn btn-warning" ng-click="onQBCleared()">{{clearButtonText}}</button> ' +
                '<button class="kcw-jqbuilder-done btn btn-success" ng-click="onQBSaved()">{{saveButtonText}}</button> ' +
                '</div>',
            scope: {
                queryBuilderConfig: '@',
                savedRules: '=',
                saveButtonText: '@',
                clearButtonText: '@',
                onSaveFn: '&',
                onClearFn: '&',
                onValidationFailFnRef: '&'
            },
            controller: function($scope, $element, $attrs) {
                $scope.qb = $element.find('.kcw-jqbuilder');
                $scope.qbConfig = (angular.isString($scope.queryBuilderConfig) && $scope.queryBuilderConfig.length > 0) ? angular.fromJson($scope.queryBuilderConfig) : null;

                //notify controller
                $scope.onQBSaved = function() {
                    $scope.savedRules = $scope.qb.queryBuilder('getRules');
                    //do we notify the controller
                    if ($scope.onSaveFn) {
                        $scope.onSaveFn();
                    }
                };
                $scope.onQBCleared = function() {
                    $scope.qb.queryBuilder('reset');
                    //now do we notify the controller
                    if ($scope.onClearFn) {
                        $scope.onClearFn();
                    }
                };
                $scope.onValidationError = function(rule, error, value, filter, operator) {
                    if ($scope.onValidationFailFnRef) {
                        $scope.onValidationFailFnRef()(rule, error, value, filter, operator);
                    }
                };

                $scope.initialize = function() {
                    //initialize the query builder

                    //add any event handlers
                    if ($scope.qbConfig) {
                        if ($scope.onValidationFailFnRef) {
                            //global validation errors
                            $scope.qbConfig.onValidationError = $scope.onValidationError;
                        }

                        //start the wiget
                        $scope.qb.queryBuilder($scope.qbConfig);
                    }

                    //load any saved query
                    if ($scope.savedRules) {
                        $scope.qb.queryBuilder('setRules', $scope.savedRules);
                    }
                };

                $scope.$watchGroup(['qbConfig', 'savedQry'], function(newVal, oldVal, scope) {
                    $scope.initialize();
                });


            }

        };
    });