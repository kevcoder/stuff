	var MakeADump = function (obj, strDelimiter, bIndent) {
	///a very simple object dumper
	if (bIndent == null) { bIndent = false; }
	if (strDelimiter == null) { strDelimiter = " => "; }
	var output = '';
	if (obj) {
	for (propetyName in obj) {
	switch (typeof obj[propetyName]) {
	case "function":
	console.log(propetyName + ' is a function');
	output += (bIndent ? " " : "") + propetyName + strDelimiter + "[function]\n";
	break;
	case "object":
	if (obj[propetyName] instanceof Date) {
	console.log(propetyName + ' is a Date');
	output += (bIndent ? " " : "") + propetyName + strDelimiter + obj[propetyName].toUTCString() + "\n";
	}
	else {
	if (obj[propetyName] instanceof Array) {
	console.log(propetyName + ' is an Array[' + obj[propetyName].length + ']');
	var copyOfArray = [];
	output += propetyName + "\n";
	//I can't iterate over obj[propetyName] and make the recursive call to MakeADump
	///so make a copy of the array and iterate over that
	for (var idx = 0; idx < obj[propetyName].length; idx++) {
	copyOfArray.push( obj[propetyName][idx] );
	}
	
	//make recursive call to get details
	for(var x = 0; x<copyOfArray.length;x++)
	output += " [" + x + "]" + strDelimiter + "\n" + MakeADump( copyOfArray[x] , strDelimiter, true) ;
	}
	else{
	console.log(propetyName + ' is a object');
	//make recursive call to get details
	output += propetyName + "\n" + MakeADump(obj[propetyName], strDelimiter, true) + "\n";
	}
	}
	break;
	case "string":
	case "number":
	console.log(propetyName + ' is ' + ( typeof propetyName));
	output += (bIndent ? " " : "") + propetyName + strDelimiter + obj[propetyName] + "\n";
	break;
	default:
	console.log(propetyName + ' is ' + ( typeof propetyName));
	output += (bIndent ? " " : "") + propetyName + strDelimiter + "[" + ( typeof propetyName) + "]\n";
	
	}
	}
	}
	return output;
	}
	
	

